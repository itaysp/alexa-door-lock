# Door Lock 
Lock and unlock the door lock using a servo motor connected to the
internet and an Alexa skill.  
This project is based on esp8266 (I use the wemos D1 mini) with a MQTT client.  


## STL Folder
Includes the stl filesl for the servo mount and gears.

## DoorLock Folder
Inclues the source code for the esp8266.  
Requirements:
* Arduino Core:  
https://github.com/esp8266/Arduino
* PubSubClient Library:  
https://github.com/knolleary/pubsubclient

## AlexaSkill Folder
Includes the skill source code (node.js) and tje speech assets.
The skill has two dependencies:
* alexa-sdk
* mqtt
 
Make sure you install them and upload them with the skill.  
They should be located in the 'node_modules' folder.  
Just make a zip file with the skill and the 'node_modules' folder,  
and upload to you AWS Lambda function. 
  
When you have node.js installed on your machine, you can install  
the dependencies with the following commands: 
```
npm install mqtt  
npm install alexa-sdk
```