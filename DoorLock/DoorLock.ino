/**********************************
*      Author: Itay Sperling      *
*              itay.sp@gmail.com  *
*      Date:  1.7.2017            * 
***********************************/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Servo.h> 

/* Wifi Settings */
const char* ssid = "YOUR_SSID";
const char* password = "YOUR_PASSWORD";

/* Mqtt Settings */
const char* mqtt_server = "YOUR_MQTT_SERVER_ADDRESS";
const char* clientUsername = "YOUR_MQTT_SERVER_USERNAME";
String clientId = "esp-door-client";
const char* clientPass = "YOUR_MQTT_SERVER_PASSWORD";
const uint16_t port = YOUR_MQTT_SERVER_PORT;
const char* topic = "DoorLock";

/* GPIOs */
const int SERVO_PIN = 2;          //Servo is connected to GPIO2

/* Objects */
WiFiClient espClient;             // Wifi client (for connecting to home network)
PubSubClient client(espClient);   // Mqtt client
Servo door_servo;                 // Servo instance

/* Settings */
const uint16_t DOOR_LOCK_DURATION = 2300;
const uint16_t DOOR_UNLOCK_DURATION = 3000;
const uint16_t DOOR_LOCK_SPEED = 50;      //from 0 to ~90
const uint16_t DOOR_UNLOCK_SPEED = 130;   //from 90 to ~180

/* Global Variables */
bool lock = false;
bool unlock = false;

/*******************************************************/

void setup() {
  Serial.begin(115200);
  setup_wifi();

  // Mqtt Initialization
  client.setServer(mqtt_server, port);
  // Set Mqtt recieve callback function
  client.setCallback(callback);

  // OTA Initialization
  initOtaUpdater();
}

void loop() {

  //Check for OTA update request
  ArduinoOTA.handle();

  //Check if Mqtt client disconnected
  if (!client.connected()) {
    reconnect();
  }

  //handle Mqtt
  client.loop();

  if(unlock)
  {
    Serial.println("\nUnlocking");
    doorUnlock();
    unlock = false;
  }

  
  if(lock)
  {
    Serial.println("\nLocking");
    doorLock();
    lock = false;
  }
}


/****************************************************/
 // Connecting to a WiFi network
void setup_wifi() {
  pinMode(LED_BUILTIN,OUTPUT);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


//Mqtt message received
void callback(char* topic, byte* payload, unsigned int length) {
  char buff[16];
  uint8_t i;
  
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  for (i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    buff[i] = (char)payload[i];
  }
  buff[i] = 0; //Termination char for working with strcmp


  if(strcmp(buff,"lock") == 0){
    lock = true;
  }
  else if((strcmp(buff,"unlock") == 0)){
    unlock = true;
  }

}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(clientId.c_str(), clientUsername, clientPass)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // ... and resubscribe
      client.subscribe(topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//Door unlock function
void doorLock(){
  //init the servo
  door_servo.attach(SERVO_PIN);

  //start pulsing the servo (for direction and speed)
  door_servo.write(DOOR_LOCK_SPEED);
  
  //wait duration seconds for the door to lock
  myDelay(DOOR_LOCK_DURATION);
  
  //stop sending pulses
  door_servo.detach(); 
}

void doorUnlock(){
  //init the servo
  door_servo.attach(SERVO_PIN);
  
  //start pulsing the servo (for direction and speed)
  door_servo.write(DOOR_UNLOCK_SPEED);
  
  //wait duration seconds for the door to unlock
  myDelay(DOOR_UNLOCK_DURATION);
  
  //stop sending pulses to the servo
  door_servo.detach(); 
}

//My delay function
void myDelay(uint32_t ms){
  uint32_t t = millis();
  while((millis() - t) < ms)
  {
    yield();            //Passes control to other tasks
    ESP.wdtFeed();      //feed the watchdog to avoid reset of the mcu
  }
}

//Init the arduino ota update functions
void initOtaUpdater()
{
  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("DoorLock");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.begin();
}






